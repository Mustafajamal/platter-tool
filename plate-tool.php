<?php
/**
 * Plugin Name:       Plate Tool
 * Plugin URI:        logicsbuffer.com/
 * Description:       Wall Plate designer Tool [platetool]
 * Version:           1.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Mustafa jamal
 * Author URI:        logicsbuffer.com/
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       plate-tool
 * Domain Path:       /languages
 */


	add_action( 'init', 'pallettool' );

	function pallettool() {

		//add_shortcode( 'show_custom_fancy_product', 'custom_fancy_product_form' );
		add_shortcode( 'platetool', 'pallet_tool_single' );
		add_action( 'wp_enqueue_scripts', 'chop_tool_script' );
		//add_action( 'wp_ajax_nopriv_post_love_calculateQuote', 'post_love_calculateQuote' );
		//add_action( 'wp_ajax_post_love_calculateQuote', 'post_love_calculateQuote' );
		// Setup Ajax action hook
		// add_action( 'wp_ajax_read_me_later', array( time(), 'read_me_later' ) );
		// add_action( 'wp_ajax_nopriv_read_me_later', array( time(), 'read_me_later' ) );
		// add_action( 'wp_ajax_read_me_later',  'read_me_later' );
		// add_action( 'wp_ajax_nopriv_read_me_later', 'read_me_later' );

		//wp_localize_script( 'my_voter_script', 'myAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));        
		//wp_enqueue_script( 'my_voter_script' );

	}
	function chop_tool_script() {
			        
		//wp_enqueue_script( 'rt_custom_fancy_font_observer', plugins_url().'/plate-tool/js/fontfaceobserver.js',array(),time());	
		wp_enqueue_script( 'lbpt_jquery', plugins_url().'/plate-tool/js/jquery-3.6.0.min.js');		
		//wp_enqueue_script( 'rt_fabric_script', plugins_url().'/plate-tool/js/fabric.js');
		wp_enqueue_script( 'lbpt_jquery_ui', plugins_url().'/plate-tool/js/jquery-ui.js');
		wp_enqueue_script( 'lbpt_main_script', plugins_url().'/plate-tool/js/mainscript.js',array(),time());	
		wp_enqueue_script( 'lbpt_fabric_script', plugins_url().'/plate-tool/js/fabric2-4.min.js');
		//wp_enqueue_script( 'pd_jcrop', plugins_url().'/plate-tool/js/jcrop.js',array(),time());
		//wp_enqueue_script( 'rt_jquery_ui', plugins_url().'/plate-tool/js/jquery-ui.js');
		//wp_enqueue_script( 'pd_cropper', plugins_url().'/plate-tool/js/cropper.min.js');
		//wp_enqueue_script( 'pd_cropper_jquery', plugins_url().'/plate-tool/js/jquery-cropper.min.js');
		
		wp_enqueue_style( 'lbpt_main_style', plugins_url().'/plate-tool/css/main.css',array(),time());
		wp_enqueue_style( 'lbpt_bootstrap', plugins_url().'/plate-tool/css/bootstrap.min.css',array(),time());
		//wp_enqueue_style( 'rt_custom_fancy_style_responsive', plugins_url().'/plate-tool/css/custom_fancy_responsive.css',array(),time());
		//wp_enqueue_style( 'rt_custom_fancy_jqeury_ui', plugins_url().'/plate-tool/css/jquery-ui.min.css',array(),time());

	}

function pallet_tool_single() {
	$plugins_url =  plugins_url( 'images/icons/', __FILE__ );
	global $post;
	global $woocommerce;
	//$product_id = $post->ID;

	if(isset($_POST['submit_prod'])){
			
		// //$total_qty = $_POST['rt_qty'];			 
		$quantity = 1;			 
		$product_id = $post->ID;
		$rt_total_price = $_REQUEST['total_price'];
		$selected_size = $_REQUEST['select_size'];
		$canvas_export = $_REQUEST['canvas_export'];
		//echo $rt_total_price;
		//$woocommerce->cart->add_to_cart( $product_id, $quantity ); 
		update_post_meta($product_id, 'total_price', $rt_total_price);
		update_post_meta($product_id, 'selected_size', $selected_size);
		update_post_meta($product_id, 'canvas_export', $canvas_export);
		// $rt_total_price = $_POST['pricetotal_quote'];
		// echo $rt_total_price;
		// echo $product_id;
		   
		//Set price
		$product_id = $post->ID;    
	    //$myPrice = get_post_meta('total_price_quote');
	    $myPrice = $rt_total_price;

	    // Get the WC_Product Object instance
		$product = wc_get_product( $product_id );

		// Set the product active price (regular)
		//$product->set_price( $rt_total_price );
		//$product->set_regular_price( $rt_total_price ); // To be sure

		// Save product data (sync data and refresh caches)
		//$product->save();
		//Set price end

		//die();
		// Cart item data to send & save in order
		$cart_item_data = array('custom_price' => $rt_total_price);   
		// woocommerce function to add product into cart check its documentation also 
		// what we need here is only $product_id & $cart_item_data other can be default.
		WC()->cart->add_to_cart( $product_id, $quantity, $variation_id, $variation, $cart_item_data);
		// // Calculate totals
		WC()->cart->calculate_totals();
		// // Save cart to session
		WC()->cart->set_session();
		// // Maybe set cart cookies
		WC()->cart->maybe_set_cart_cookies();	    

	}
	ob_start();
	//$page_title = get_the_title();
	//$terms = get_the_terms( get_the_ID(), 'product_cat' );
	//$product_type = $terms[0]->slug;
	?>

	<form role="form" action="" method="post" id="add_cart" enctype="multipart/form-data" method="post">						

<div id="choptoolmain" class="custom_calculator single_product">

	   <div class="container">
	      <header class="scroll" id="imagesHeader">
	         <span class="matleftarrow" onclick="scrollImgLeft();" style="margin-top: 30px;"><b id="imgLeftArrow">‹</b></span>
	         <nav class="mvertical-align-middle" id="imageList">
	            <!-- <span class="mnav-thumb" id="imageTab8389"><img src="http://localhost/stepform/wp-content/uploads/2021/05/app.jpeg" style="max-height: 120px; max-width: 200px;" onclick="setDefaultImage(8389);"></span>  -->         
	         </nav>
	        <div class="categoryicons">
				<div class="platteimagegal" >
					<?php echo do_shortcode('[ngg src="galleries" ids="7" display="basic_thumbnail" override_thumbnail_settings="1" thumbnail_width="180" thumbnail_height="120" images_per_page="6" ajax_pagination="0" thumbnail_crop="0" number_of_columns="6"]');?>
				</div>
			</div>
	      </header>
	   </div>
	  	<div class="container">
		  	<div class="row">
				<div class="col-2 col-sm-12 col-md-2 col-lg-2">
					<div id="detailSelect">
						<span style="float: right; background-color: #284156; font-size: 14px; border-radius: 10px; color: #FFFFFF; width: 18px; text-align: center; cursor: pointer;" onclick="initHelp('material')"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">?</font></font></span>
						<b><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MATERIAL</font></font></b><br>
						<div class="matbox" id="mat1" onclick="matSelect('1');" style="cursor: pointer;">
						<font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Alu Premium Nano matt</font>
						</font>
						</div>
						<div class="matbox" id="mat2" onclick="matSelect('2');" style="cursor: pointer;">
						<font style="vertical-align: inherit;">
						<font style="vertical-align: inherit;">Alu premium nano gloss</font>
						</font>
						</div>
						<div class="matbox matactive" id="mat4" onclick="matSelect('4');" style="cursor: pointer;">
						<font style="vertical-align: inherit;">
						<font style="vertical-align: inherit;">
						Alu metal look nano                    
						</font>
						</font>
						</div>
						<div class="matbox" id="mat21" onclick="matSelect('21');" style="cursor: pointer;">
						<font style="vertical-align: inherit;">
						<font style="vertical-align: inherit;">Forex® Classic Nano</font>
						</font>
						</div>
						<input type="hidden" name="materialId" id="materialId" value="4">
					</div>
					<div class="plateselect">
						<div class="platecount" id="platec1" data-val="1"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">1</font></font></div>
						<div class="platecount" id="platec2" data-val="2"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2</font></font></div>
						<div class="platecount plateactive" id="platec3" data-val="3"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">3</font></font></div>
					</div>
			    </div>
			    <div class="col-8 col-sm-12 col-md-8 col-lg-8 text-center mainarea" style="white-space: nowrap;" id="configCenter">
			      <!--
			      <button class="btn btn-sm" style="border: 1px solid #666666; font-size: 0.8em; margin-top: 4px;" id="myBtn" onclick="cutService();">Ausschnitt wählen / Spiegeln</button>
			      &nbsp; &nbsp;
			      <button class="btn btn-sm" style="border: 1px solid #888888; font-size: 0.8em; margin-top: 4px; color: #666666;" id="myReset" onclick="clearSettings();">Zurücksetzen</button>
			  		-->	
			  	  <!--<div id="#demo"><img id="crpped1" src="http://localhost/stepform/wp-content/uploads/2021/05/app.jpeg"></div>-->	
			      
			      <div style="font-size: 12px; margin-bottom: 10px;">
			         Höhe: <input type="number" style="max-width: 62px; font-size: 14px; padding: 2px; margin: 2px; text-align: center;" id="pHeight" value="2000" size="4" maxlength="4"> mm<br>
			         <div class="cmstyle" style="margin-top: -5px;" id="pHeightcm">150 cm</div>
			      </div>
			      <canvas id="canvas" ></canvas>

			      <div align="center" id="mainplateswrap">
			         <span class="verticalImage2" id="vcol0" style="max-width: 93%; text-align: right;">
			         	<img src="" id="pimg0" style="max-height: 580px; height: 579.875px;">
			         </span>
			         <span class="verticalImage2" id="vcol1" style="max-width: 6%; text-align: left;">
			         	<img src="" id="pimg1" style="max-height: 580px; height: 579.875px;">
			         </span>
			         <span class="verticalImage3" id="vcol2" style="display: none;">
			         	<img src="maker/blank.png" id="pimg2">
			         </span>
			      </div>
			      <div align="center" id="imageName" class="cmstyle">Shop_BS_09_20.jpg</div>
			      <div align="center">
			         <span style="font-size: 12px; width: 50%;" class="verticalImage2" id="wInput0">
			            <span>Breite:</span><br>
			            <input type="number" style="max-width: 62px; font-size: 14px; padding: 2px; margin: 2px; text-align: center;" value="900" id="width0" size="4" maxlength="4"> mm
			            <div class="cmstyle" style="margin-top: -5px;" id="width0cm">150 cm</div>
			         </span>
			         <span style="font-size: 12px; width: 50%;" class="verticalImage2" id="wInput1">
			            <span>Breite:</span><br>
			            <input type="number" style="max-width: 62px; font-size: 14px; padding: 2px; margin: 2px; text-align: center;" value="900" id="width1" size="4"> mm
			            <div class="cmstyle" style="margin-top: -5px;" id="width1cm">10 cm</div>
			         </span>
			         <span style="font-size: 12px; display: none;" class="verticalImage3" id="wInput2">
			            <span>Breite:</span><br>
			            <input type="number" style="max-width: 62px; font-size: 14px; padding: 2px; margin: 2px; text-align: center;" value="900" id="width2" size="4"> mm
			            <div class="cmstyle" style="margin-top: -5px;" id="width2cm">90 cm</div>
			         </span>
			      </div>
			    </div>
			    <div class="col-2 col-sm-12 col-md-2 col-lg-2">
			    </div>
		    </div>
	    </div>
	
</div>

	</form>			
	<?php 
	return ob_get_clean();
}
	function platetool(){
		echo do_shortcode("[platetool]");
	}
	add_action( 'woocommerce_after_single_product', 'platetool', 20 );
