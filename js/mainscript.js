jQuery(document).ready(function() {
// 	(function() {
    var canvas = new fabric.Canvas('canvas');
	var bg_default_image = "http://localhost/stepform/wp-content/uploads/2021/05/app.jpeg";

    //Change Platte
    jQuery( "#platec1,#platec2,#platec3" ).click(function(e) {
        changePlates(jQuery(this).attr("data-val"));
    });
    jQuery( ".platteimagegal a" ).click(function(e) {
        //alert(jQuery(this).attr('data-val'));
        setTimeout(function(){ 
            jQuery("html").removeClass("has-lightbox");
            jQuery("body").removeClass("hidden-scroll");
        }, 900);
        

        e = e || window.event;
        e.preventDefault();
        e.stopPropagation();
        
        var clicked_image  = jQuery(this).attr('href');
        bg_default_image = clicked_image;
        console.log("clicked_image.."+clicked_image);
        localStorage.setItem("choosenimg",clicked_image);
        updateView();
        checkDims();
    });

	// canvas.setBackgroundImage(bg_image, canvas.renderAll.bind(canvas),{
	// 	top: parseInt(side_a_bg_top),
	// 	left: parseInt(side_a_bg_left), 
	// 	scaleX: parseInt(side_a_bg_scale),
	// 	scaleY: 1		
	// });
// })();
// });
/*
    maker.js for plateART.de

    Heavily intertwined with the PHP part of the app.
    Handles cutting, selecting and moving of the image
    on different materials.

    CAUTION: heavy mix of synchronous, asynchronous and global variable use
    (c) by rs(at)metadist.de - October 2020
*/
// ------------------------------------------------------- GLOBAL VARIABLES
// ---------------- define graphical values

var motherSnippet = [0,0,100,100];
var defaultSizesMinMax = [];
defaultSizesMinMax["standard"] = [100,100,1500,3000];
defaultSizesMinMax["kitchen"] = [100,100,3000,1500];
defaultSizesMinMax["quattro"] = [100,100,1000,3000];

var defaultPixelHeight = [];
defaultPixelHeight["standard"] = 580;
defaultPixelHeight["kitchen"] = 560;
defaultPixelHeight["quattro"] = 500;

var defaultType = "standard";  // standard 4500x3000 - kitchen 3000x1500 - quattro 3000x3000
var plateCount = 2;

var minMaxMM = [];
minMaxMM["standard"] = [100,100,1500,3000];
minMaxMM["kitchen"] = [100,100,3000,1500];
minMaxMM["quattro"] = [100,100,1000,3000];

var defPlateCount = [];
defPlateCount["standard"] = 2;
defPlateCount["kitchen"] = 1;
defPlateCount["quattro"] = 2;

var defSizes = [];
defSizes["standard"] = [900,2000];
defSizes["kitchen"] = [2000,600];
defSizes["quattro"] = [900,2000];

var plateStartDefault = [0,0];
var totalPlatesWidth = defaultSizesMinMax[defaultType][2]*3;
var totalPlatesHeight = defaultSizesMinMax[defaultType][3];

var pshare = [];

var flipMother = 0;
var zoomMother = 1;
//
var fullWidth = minMaxMM[defaultType][2]*3;
var fullHeight = minMaxMM[defaultType][3];

var showWidth = 100;
var showHeight = 100;

var motherRatio = fullWidth / fullHeight;
var selectionRatio = fullWidth / fullHeight;

// ----------------------------------------------------------------- snipper settings
var startPercentX = 0;
var startPercentY = 0;
var endPercentX = 100;
var endPercentY = 100;

var displayWidth = 640;
var displayHeight = 400;

// selection box
var snipX = 0, snipY = 0, snipW = 0, snipH = 0;
var startZoom = 0;

// selectionRation smaller than motherRation? then Full Height used!
// else full Width used!
var relSnipStartXPercent = 0;
var relSnipStartYPercent = 0;


jQuery("#width0,#width1,#width2,#pHeight").on("change", function(e) {
    checkDims();
})
jQuery("#pimg0,#pimg1,#pimg2").on("keyup change", function(e) {
    equalizeHeight();
})

// ------------------------------------------------------------------------------------------------
// main functions
// ------------------------- calc start values
function calcStartValues() {
    getTotalPlatesWidth();

    zoomMother = 1;
    fullWidth = defaultSizesMinMax[defaultType][2]*3;
    fullHeight = defaultSizesMinMax[defaultType][3];

    if(defaultType=="quattro") {
        motherRatio = 1;
    } else if(defaultType=="standard") {
        motherRatio = 1.5;
    } else {
        motherRatio = 4;
    }
    selectionRatio = totalPlatesWidth / totalPlatesHeight;

    if (selectionRatio < motherRatio) {
        // full height
        showWidth = parseFloat((100 / motherRatio * selectionRatio)).toFixed(4);
        relSnipStartXPercent = 50 - parseFloat((showWidth / 2)).toFixed(4);
    } else if (selectionRatio > motherRatio) {
        // full width
        showHeight = parseFloat((100 / selectionRatio * motherRatio)).toFixed(4);
        relSnipStartYPercent = 50 - parseFloat((showHeight / 2)).toFixed(4);
    } else {
        showWidth = 100;
        showHeight = 100;
        relSnipStartXPercent = 0;
        relSnipStartYPercent = 0;
    }
    plateStartDefault = [relSnipStartXPercent,relSnipStartYPercent];
}
// ------------------------------------------------------------------------------------------------
// put the current selection into the wooCommerce basket via app.php
function addConfig2Basket() {
    // var plate;
    // var pw = [];

    // pw[0] = jQuery("#width0").val();
    // jQuery("#width0cm").html(pw[0]/10 + " cm");

    // pw[1] = jQuery("#width1").val();
    // jQuery("#width1cm").html(pw[1]/10 + " cm");

    // pw[2] = jQuery("#width2").val();
    // jQuery("#width2cm").html(pw[2]/10 + " cm");

    // // take width calculations per plate centered from image
    // totalPlatesWidth = getTotalPlatesWidth();

    // // calc part of each plate, height is calculated in PHP from gh
    // var pshare = [];
    // for(plate=0; plate < plateCount; plate++) {
    //     pshare[plate] = parseFloat((pw[plate] / totalPlatesWidth * 100).toFixed(4));
    // }
    // /* DEBUG
    // //console.log("defaultImage: " + defaultImage.toString());
    // //console.log("defaultMat: " + defaultMat.toString());
    // //console.log("defaultOrient: " + defaultType.toString());
    // //console.log("MotherSnippet: " + motherSnippet.join(","));
    // //console.log("Plate(s): " + plateCount.toString());
    // //console.log("Height: " + jQuery("#pHeight").val().toString());
    // //console.log("PlateSizes: " + jQuery("#width0").val().toString() + "_" + jQuery("#width1").val().toString() + "_" +  jQuery("#width2").val().toString());
    // */
    // var cImage = defaultImage.toString();
    // var cMat = defaultMat.toString();
    // var cCat = myCatId.toString();
    // var cOrient = defaultType.toString();
    // var cMother = motherSnippet.join(",");
    // var cRelstart = plateStartDefault.join(",");
    // var cPlates = plateCount.toString();
    // var cZoom = zoomMother.toString();
    // var cFlip = flipMother.toString();
    // var cShares = pshare.join(",");
    // var cHeight = jQuery("#pHeight").val().toString();
    // var cSizes =  jQuery("#width0").val().toString() + "," + jQuery("#width1").val().toString() + "," +  jQuery("#width2").val().toString();
    // var cService = jQuery("input[name='snippetService']:checked").val();

    // var bUrl = defaultImage;
    // bUrl = bUrl + "&cImage="+cImage;
    // bUrl = bUrl + "&cMat="+cMat;
    // bUrl = bUrl + "&cCat="+cCat;
    // bUrl = bUrl + "&cOrient="+cOrient;
    // bUrl = bUrl + "&cMother="+cMother;
    // bUrl = bUrl + "&cRelstart="+cRelstart;
    // bUrl = bUrl + "&cPlates="+cPlates;
    // bUrl = bUrl + "&cSizes="+cSizes;
    // bUrl = bUrl + "&cZoom=" + cZoom;
    // bUrl = bUrl + "&cFlip=" + cFlip;
    // bUrl = bUrl + "&cShares=" + cShares;
    // bUrl = bUrl + "&cHeight="+cHeight;
    // bUrl = bUrl + "&cService="+cService;

    // // //console.log(bUrl);

    // jQuery.getJSON(bUrl, function(data) {
    //     var pId = data.productId;
    //     var pUrl = data.productUrl;
    //     jQuery('#customBasket').attr('action', urlRel+'shop/');
    //     jQuery("#addCartId").val(pId);
    //     jQuery('#customBasket').submit();
    // });
}
// put the current selection into the wooCommerce basket via app.php
function addMuster2Basket() {
    // var plate;
    // var pw = [];

    // pw[0] = 300;

    // // take width calculations per plate centered from image
    // totalPlatesWidth = 300;

    // // calc part of each plate, height is calculated in PHP from gh
    // var pshare = [];
    // for(plate=0; plate < 1; plate++) {
    //     pshare[plate] = parseFloat((pw[plate] / totalPlatesWidth * 100).toFixed(4));
    // }
    // /* DEBUG
    // //console.log("defaultImage: " + defaultImage.toString());
    // //console.log("defaultMat: " + defaultMat.toString());
    // //console.log("defaultOrient: " + defaultType.toString());
    // //console.log("MotherSnippet: " + motherSnippet.join(","));
    // //console.log("Plate(s): " + plateCount.toString());
    // //console.log("Height: " + jQuery("#pHeight").val().toString());
    // //console.log("PlateSizes: " + jQuery("#width0").val().toString() + "_" + jQuery("#width1").val().toString() + "_" +  jQuery("#width2").val().toString());
    // */
    // // var cImage = 0;

    // var cImage = defaultImage.toString();
    // var cMat = defaultMat.toString();
    // var cCat = myCatId.toString();
    // var cOrient = "standard";
    // var cMother = motherSnippet.join(",");
    // var cRelstart = plateStartDefault.join(",");
    // var cPlates = 1;
    // var cZoom = zoomMother.toString();
    // var cFlip = flipMother.toString();
    // var cShares = pshare.join(",");
    // var cHeight = "200";
    // var cSizes =  "300,0,0";
    // var cService = "SelfService";

    // var bUrl = defaultImage;
    // bUrl = bUrl + "&cImage="+cImage;
    // bUrl = bUrl + "&cMat="+cMat;
    // bUrl = bUrl + "&cCat="+cCat;
    // bUrl = bUrl + "&cOrient="+cOrient;
    // bUrl = bUrl + "&cMother="+cMother;
    // bUrl = bUrl + "&cRelstart="+cRelstart;
    // bUrl = bUrl + "&cPlates="+cPlates;
    // bUrl = bUrl + "&cSizes="+cSizes;
    // bUrl = bUrl + "&cZoom=" + cZoom;
    // bUrl = bUrl + "&cFlip=" + cFlip;
    // bUrl = bUrl + "&cShares=" + cShares;
    // bUrl = bUrl + "&cHeight="+cHeight;
    // bUrl = bUrl + "&cService="+cService;
    // bUrl = bUrl + "&muster=true";

    // //alert(bUrl);
    // // //console.log(bUrl);

    // jQuery.getJSON(bUrl, function(data) {
    //     var pId = data.productId;
    //     var pUrl = data.productUrl;
    //     jQuery('#customBasket').attr('action', urlRel+'shop/');
    //     jQuery("#addCartId").val(pId);
    //     jQuery('#customBasket').submit();
    // });
}
// ------------------------------------------------------------------------------------------------
function initSnipper() {
    ////console.log("Init: " + flipMother);
    jQuery("#cropBox").load(defaultImage+"maker/snipper.php?id="+defaultImage.toString()+"&flip="+flipMother.toString()+"&touring=" + Math.random().toString(10));
}
//
// function initHelp(ident) {
//     jQuery("#modalTitle").html("Hilfe zu: "+ ident.toUpperCase());
//     modal.style.display = "inline-block";
//     modal.style.position = "fixed";
//     modal.style.zIndex = "99";
//     jQuery(".section-content").css({"z-index":"auto"});
//     jQuery("#cropBox").load(defaultImage+"maker/app.php?action=help&ident="+ident);
// }

// ------------------------------------------------------------------------------------------------
function checkDims() {
    var totalHeight = jQuery("#pHeight").val();
    // set height
    // previous
    /*
    if(1==2) {
        if (totalHeight < minMaxMM[defaultType][1]) {
            alert("Mindesthöhe: " + minMaxMM[defaultType][1].toString());
            jQuery("#pHeight").val(minMaxMM[defaultType][1]);
        }
        if (totalHeight > minMaxMM[defaultType][3]) {
            alert("Maximalhöhe: " + minMaxMM[defaultType][3].toString());
            jQuery("#pHeight").val(minMaxMM[defaultType][3]);
        }
        // ---------------------
        var plate = 0;
        var pw = [];
        pw[0] = jQuery("#width0").val();
        pw[1] = jQuery("#width1").val();
        pw[2] = jQuery("#width2").val();

        for (plate = 0; plate < plateCount; plate++) {
            if (pw[plate] < minMaxMM[defaultType][0]) {
                alert("Mindestbreite: " + minMaxMM[defaultType][0].toString());
                jQuery("#width" + plate.toString()).val(minMaxMM[defaultType][0]);
            }
            if (pw[plate] > minMaxMM[defaultType][2]) {
                alert("Maximalbreite: " + minMaxMM[defaultType][2].toString());
                jQuery("#width" + plate.toString()).val(minMaxMM[defaultType][2]);
            }
        }
    }
    */
    // fix checks if 4500x3000 is busted in any direction
    var dimHintText = "Unsere Platten können auf von 100 x 100 bis zu 3.000 x 1.500 Millimeter geschnitten werden.\nWenn Ihre Höhe 1.500 überschreitet, liegt die maximale Breite bei 1.500 und entsprechend für die Breite ebenso.";
    var plate = 0;
    var pw = [];
    var platesTotalWidthSum = 0;
    var showProfiHint = true;

    pw[0] = jQuery("#width0").val();
    pw[1] = jQuery("#width1").val();
    pw[2] = jQuery("#width2").val();

    if(totalHeight > 3000) {
        alert(dimHintText);
        jQuery("#pHeight").val(3000);
        totalHeight = 3000;
    }
    if(totalHeight < 100) {
        alert(dimHintText);
        jQuery("#pHeight").val(100);
        totalHeight = 100;
    }

    for (plate = 0; plate < plateCount; plate++) {
        if (pw[plate] < minMaxMM[defaultType][0]) {
            alert("Mindestbreite: " + minMaxMM[defaultType][0].toString());
            jQuery("#width" + plate.toString()).val(minMaxMM[defaultType][0]);
        }
        if(totalHeight > 1500) {
            if (pw[plate] > 1500) {
                alert("Maximalbreite: 1.500 mm!\n\n" + dimHintText);
                jQuery("#width" + plate.toString()).val(1500);
                showProfiHint = false;
            }
        } else {
            if (pw[plate] > 3000) {
                alert("Maximalbreite: 3.000 mm!\n\n" + dimHintText);
                jQuery("#width" + plate.toString()).val(3000);
            }
        }
        platesTotalWidthSum = platesTotalWidthSum + parseInt(pw[plate],10);
    }

    if((platesTotalWidthSum > 4500) && showProfiHint==true) {
        alert("Bei einer Gesamtbreite von über 4.500 mm (4,5m) müssen wir zu Qualitätskontrolle des Bildes (Auflösung) den Profi-Schnitt aktivieren.");
        setService('ProfiSnippet');
    }
    // MINIMAL = 200x300
    if((totalHeight*platesTotalWidthSum < 60000)) {
        alert("Die Gesamtfläche unterschreitet 60 cm² !");
        jQuery("#pHeight").val(200);
        jQuery("#width0").val(300);
    }

    // update view and set values
    zoomMother = 1;
    totalPlatesWidth = getTotalPlatesWidth();

    changePlates(plateCount);
}
// ------------------------------------------------------------------------------------------------
function getTotalPlatesWidth() {
    var plate = 0;
    totalPlatesWidth = 0;
    totalPlatesHeight = parseInt(jQuery("#pHeight").val(),10);
    var pw = [];
    pw[0] = parseInt(jQuery("#width0").val(),10);
    pw[1] = parseInt(jQuery("#width1").val(),10);
    pw[2] = parseInt(jQuery("#width2").val(),10);
    for(plate=0; plate < plateCount; plate++) {
        totalPlatesWidth = totalPlatesWidth + pw[plate];
    }
    return totalPlatesWidth;
}
// ------------------------------------------------------------------------------------------------
function updatePrice() {
    // var materialId = jQuery("#materialId").val();
    // var totalHeight = parseInt(jQuery("#pHeight").val(),10);
    // var totalQm = 0;
    // var snipService = jQuery("input[name='snippetService']:checked").val();

    // // sum up the plates that are used
    // var pw = [];

    // pw[0] = jQuery("#width0").val();
    // jQuery("#width0cm").html(pw[0]/10 + " cm");

    // pw[1] = jQuery("#width1").val();
    // jQuery("#width1cm").html(pw[1]/10 + " cm");

    // pw[2] = jQuery("#width2").val();
    // jQuery("#width2cm").html(pw[2]/10 + " cm");

    // // take width calculations per plate centered from image
    // totalPlatesWidth = getTotalPlatesWidth();

    // totalQm = totalHeight * totalPlatesWidth / (1000*1000);

    // jQuery.getJSON(urlRel+"maker/app.php?action=getPrice&matId=" + materialId.toString() + "&qm=" + totalQm.toString() + "&serv=" + snipService.toString(), function(data) {
    //    jQuery("#priceValue").html(new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(data.price));
    // });

    // updateMusterPrice();
}
// ------------------------------------------------------------------------------------------------
function updateMusterPrice() {
    // var materialId = jQuery("#materialId").val();
    // var totalHeight = 200;
    // var totalQm = 0;
    // var snipService = "SelfSnippet";

    // // sum up the plates that are used
    // var pw = [];

    // // take width calculations per plate centered from image
    // // totalPlatesWidth = getTotalPlatesWidth();

    // totalQm = 200 * 300 / (1000*1000);

    // jQuery.getJSON(urlRel+"maker/app.php?action=getPrice&matId=" + materialId.toString() + "&qm=" + totalQm.toString() + "&serv=" + snipService.toString(), function(data) {
    //     jQuery("#musterprice").html(new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(data.price));
    // });
}

// ------------------------------------------------------------------------------------------------
function toogleMatSelectors(arrKeys) {
    var loopCount = 0;
    var matId = 0;
    var VisibleIds = "";
    var HiddenIds = "";
    var latestVisibleMatId = 0;

    jQuery("#detailSelect").children("div").each(function(index) {
        activeDivText = jQuery(this).text();
        activeDivId = jQuery(this).attr("id");
        matId = parseInt(activeDivId.replace("mat",""));

        jQuery("#" + activeDivId).show();

        //console.log("Active DIV id: " + activeDivId + " -- ");
        //console.log("Active text: " + activeDivText + " in DIV");

        if(arrKeys.length > 0) {
           var hideDiv = true;
           arrKeys.forEach(function(allowedText) {
               allowedText = allowedText.replace("Material: ","");
               //console.log("Allowed is: " + allowedText);
               if(activeDivText.search(allowedText) > -1) {
                   hideDiv = false;
                   //console.log("Searching " + allowedText + " in " + activeDivText + " - MatID: " + latestVisibleMatId);
                   if(latestVisibleMatId==0) {
                       latestVisibleMatId = matId;
                   }
               }
           });

           if(hideDiv) {
               jQuery("#" + activeDivId).hide();
               HiddenIds = HiddenIds + "|"+matId+"|";
           }
        }
        loopCount++;
    });

    if(HiddenIds.search("|"+defaultMat+"|") > -1 && latestVisibleMatId>0) {
        matSelect(latestVisibleMatId);
    }
}

// ------------------------------------------------------------------------------------------------
function setDefaultImage(id) {
    defaultImage = id;
    myThumbId = id;
    ////console.log(id);
    // ------------------------------------------------------------------
    // set default type to quattro, if tag is set:
    //jQuery.getJSON(urlRel+"http://localhost/stepform/wp-content/uploads/2021/05/app.jpeg" + defaultImage.toString(), function(data) {
    jQuery.getJSON("http://localhost/stepform/wp-content/uploads/2021/05/app.jpeg", function(data) {
        defaultType = data["orientation"];
        matLimit = data["materialLimit"];
        toogleMatSelectors(matLimit);
        //alert(defaultType);
        motherSnippet = [0,0,100,100];
        plateStartDefault = [0,0];
        zoomMother = 1;
        flipMother = 0;
        selectionRatio = fullWidth / fullHeight;

        // ----------------------------------------------------------------- snipper settings
        startPercentX = 0;
        startPercentY = 0;
        endPercentX = 100;
        endPercentY = 100;

        zoomMother = 1;
        fullWidth = minMaxMM[defaultType][2]*3;
        fullHeight = minMaxMM[defaultType][3];

        motherRatio = fullWidth / fullHeight;

        showWidth = 100;
        showHeight = 100;

        //selection box
        snipX = 0, snipY = 0, snipW = 0, snipH = 0;

        startZoom = 0;
        // selectionRation smaller than motherRation? then Full Height used!
        // else full Width used!
        relSnipStartXPercent = 0;
        relSnipStartYPercent = 0;

        jQuery("#platec3").prop("checked", true);
        jQuery("#imageName").html(data["filename"]);
        changePlates(plateCount);
    });
}
// ------------------------------------------------------------------------------------------------
function matSelect(myId) {
    jQuery("#detailSelect").children().removeClass("matactive");
    jQuery("#mat"+myId.toString()).addClass("matactive");
    jQuery("#materialId").val(myId);
    defaultMat = myId;
    ////console.log("Material: " + jQuery("#materialId").val());
    updatePrice();
}
// ------------------------------------------------------------------------------------------------
function catSelect(myId) {
    myCatId = myId;
    motherSnippet = [0,0,100,100];

    //console.log("Coming from: "+defaultType);

    jQuery("#catNav").children().removeClass("mactive");
    jQuery("#cat" + myId.toString()).addClass("mactive");

    zoomMother = 1;
    flipMother = 0;

    //jQuery("#imageList").load(urlRel+"maker/app.php?action=getImageList&id=" + myId.toString() + "&myThumbId=" + (0 + myThumbId).toString());
    jQuery("#imageList").load(bg_default_image);
}
// ------------------------------------------------------------------------------------------------
function setMMDefaults() {
    // //console.log("Height: "+ defaultType + ": " + defSizes[defaultType][1]);
    jQuery("#width0").val(defSizes[defaultType][0]);
    jQuery("#width1").val(defSizes[defaultType][0]);
    jQuery("#width2").val(defSizes[defaultType][0]);
    jQuery("#pHeight").val(defSizes[defaultType][1]);

    totalPlatesWidth = getTotalPlatesWidth();

    //reset ratio
    //motherRatio = (defaultSizesMinMax[defaultType][2] * 3) / defaultSizesMinMax[defaultType][3];
    if(defaultType=="quattro") {
        motherRatio = 1;
    } else if(defaultType=="standard") {
        motherRatio = 1.5;
    } else {
        motherRatio = 4;
    }


    selectionRatio = totalPlatesWidth / totalPlatesHeight;
    ////console.log("**** DefaultRatio:"+motherRatio);
    changePlates(plateCount);
}
// ------------------------------------------------------------------------------------------------
function    (myCount) {

    plateCount = myCount;
    zoomMother = 1;

    var gh = parseInt(jQuery("#pHeight").val(),10);
    totalPlatesWidth = getTotalPlatesWidth();

    jQuery("#platec1").removeClass("plateactive");
    jQuery("#platec2").removeClass("plateactive");
    jQuery("#platec3").removeClass("plateactive");

    jQuery("#platec" + plateCount.toString()).addClass("plateactive");

    // percentage width of spans to allow bigger images for 1 and 2 plates
    var maxW0 = parseInt(jQuery("#width0").val() / totalPlatesWidth * 99.5 ,10);
    var maxW1 = parseInt(jQuery("#width1").val() / totalPlatesWidth * 99.5 ,10);
    var maxW2 = parseInt(jQuery("#width2").val() / totalPlatesWidth * 99.5 ,10);

    // get ratio and set start X and start Y
    // show / hide and change classes by amount of plates
    if(parseInt(plateCount, 10) === 3) {

        jQuery("#vcol0").removeClass("verticalImage2");
        jQuery("#vcol0").removeClass("verticalImage1");
        jQuery("#vcol0").addClass("verticalImage3");
        jQuery("#vcol0").css("max-width", maxW0+"%");
        jQuery("#vcol0").css("text-align","right");

        jQuery("#vcol1").removeClass("verticalImage2");
        jQuery("#vcol1").removeClass("verticalImage1");
        jQuery("#vcol1").addClass("verticalImage3");
        jQuery("#vcol1").css("max-width", maxW1+"%");
        jQuery("#vcol1").css("text-align","center");

        jQuery("#vcol2").removeClass("verticalImage2");
        jQuery("#vcol2").removeClass("verticalImage1");
        jQuery("#vcol2").addClass("verticalImage3");
        jQuery("#vcol2").css("max-width", maxW2+"%");
        jQuery("#vcol2").css("text-align","left");

        jQuery("#vcol2").show();
        jQuery("#vcol1").show();

        // winputs
        jQuery("#wInput0").removeClass("verticalImage2");
        jQuery("#wInput0").removeClass("verticalImage1");
        jQuery("#wInput0").addClass("verticalImage3");
        jQuery("#wInput0").css("width","30%");

        jQuery("#wInput1").removeClass("verticalImage2");
        jQuery("#wInput1").removeClass("verticalImage1");
        jQuery("#wInput1").addClass("verticalImage3");
        jQuery("#wInput1").css("width","30%");

        jQuery("#wInput2").removeClass("verticalImage2");
        jQuery("#wInput2").removeClass("verticalImage1");
        jQuery("#wInput2").addClass("verticalImage3");
        jQuery("#wInput2").css("width","30%");

        jQuery("#wInput1").show();
        jQuery("#wInput2").show();

        plateStartDefault = [0, plateStartDefault[1]];
    }
    if(parseInt(plateCount, 10) === 2) {
        jQuery("#vcol0").removeClass("verticalImage3");
        jQuery("#vcol0").removeClass("verticalImage1");
        jQuery("#vcol0").addClass("verticalImage2");
        jQuery("#vcol0").css("max-width", maxW0+"%");
        jQuery("#vcol0").css("text-align","right");

        jQuery("#vcol1").removeClass("verticalImage3");
        jQuery("#vcol1").removeClass("verticalImage1");
        jQuery("#vcol1").addClass("verticalImage2");
        jQuery("#vcol1").css("max-width", maxW1+"%");
        jQuery("#vcol1").css("text-align","left");

        jQuery("#vcol2").hide();
        jQuery("#vcol1").show();

        // winputs
        jQuery("#wInput0").removeClass("verticalImage3");
        jQuery("#wInput0").removeClass("verticalImage1");
        jQuery("#wInput0").addClass("verticalImage2");
        jQuery("#wInput0").css("width","50%");

        jQuery("#wInput1").removeClass("verticalImage3");
        jQuery("#wInput1").removeClass("verticalImage1");
        jQuery("#wInput1").addClass("verticalImage2");
        jQuery("#wInput1").css("width","50%");

        jQuery("#wInput2").hide();
        jQuery("#wInput1").show();

        plateStartDefault = [16, plateStartDefault[1]];
    }
    if(parseInt(plateCount, 10) === 1) {
        jQuery("#vcol0").removeClass("verticalImage2");
        jQuery("#vcol0").removeClass("verticalImage3");
        jQuery("#vcol0").addClass("verticalImage1");
        jQuery("#vcol0").css("max-width", maxW0+"%");
        jQuery("#vcol0").css("text-align","center");
        jQuery("#wInput0").css("width","99%");

        jQuery("#vcol2").hide();
        jQuery("#vcol1").hide();

        jQuery("#wInput2").hide();
        jQuery("#wInput1").hide();
        plateStartDefault = [33, plateStartDefault[1]];
    }

    // selectionRation smaller than motherRatio? then Full Height used!
    // else full Width used!
    var relSnipStartXPercent = 0;
    var relSnipStartYPercent = 0;

    selectionRatio = totalPlatesWidth / gh;

    if(selectionRatio < motherRatio) {
        // full height
        showWidth =  parseFloat((100 / motherRatio * selectionRatio)).toFixed(4);
        relSnipStartXPercent = 50 - parseFloat((showWidth / 2)).toFixed(4);
    } else if(selectionRatio > motherRatio) {
        // full width
        showHeight =  parseFloat((100 / selectionRatio * motherRatio)).toFixed(4);
        relSnipStartYPercent = 50 - parseFloat((showHeight / 2)).toFixed(4);
    }
    plateStartDefault = [relSnipStartXPercent,relSnipStartYPercent];
    //console.log(plateStartDefault);
    updateView();
}
// ------------------------------------------------------------------------------------------------
    //var selectedplates = 2;
    localStorage.setItem("selectedplates",2);
    //Get plate Counts
    jQuery("div.platecount").click(function() {
        var selectedplates = jQuery(this).attr('data-val');
        localStorage.setItem("selectedplates",selectedplates);
        console.log("selectedplates.."+selectedplates);
        updateView();
    });
function updateView(){
    var plate;
    var p , pdomId;
    var gh;
    var pw = [];

    gh = parseInt(jQuery("#pHeight").val(),10);
    jQuery("#pHeightcm").html(gh/10 + " cm");

    pw[0] = jQuery("#width0").val();
    jQuery("#width0cm").html(pw[0]/10 + " cm");

    pw[1] = jQuery("#width1").val();
    jQuery("#width1cm").html(pw[1]/10 + " cm");

    pw[2] = jQuery("#width2").val();
    jQuery("#width2cm").html(pw[2]/10 + " cm");

    // take width calculations per plate centered from image
    totalPlatesWidth = getTotalPlatesWidth();
    //console.log("totalPlatesWidth.."+totalPlatesWidth);
    // calc part of each plate
    // height is calculated in PHP from gh
    for(plate=0; plate < plateCount; plate++) {
        pshare[plate] = parseFloat((pw[plate] / totalPlatesWidth * 100)).toFixed(4);
        var first_pallet_left = parseFloat(pshare[0]) / 100 * parseFloat(pw[0]);
        var second_pallet_left = parseFloat(pshare[1]) / 100 * parseFloat(pw[1]);
        console.log("pshare[2].."+pshare[2]);
        console.log("pshare[2].."+pw[2]);
        var third_pallet_left = parseFloat(pshare[2]) / 100 * parseFloat(pw[2]);
        console.log("first_pallet_left",first_pallet_left);
        console.log("second_pallet_left",second_pallet_left);
        console.log("third_pallet_left",third_pallet_left);
        var second_pallet_total = parseFloat(first_pallet_left) + parseFloat(second_pallet_left);
        var third_pallet_total = parseFloat(first_pallet_left) + parseFloat(second_pallet_left) + parseFloat(third_pallet_left);
        console.log("third_pallet_total.."+third_pallet_total);

        var selectedplates = localStorage.getItem("selectedplates");
        if (selectedplates == 1){
            canvas.remove(canvas.item(0));
            canvas.remove(canvas.item(1));
            canvas.remove(canvas.item(2));
            canvas.remove(canvas.item(3));
            //First Pallet   
            var boundingbox1 = new fabric.Rect({
                left: parseInt(first_pallet_left),
                top: 0,
                width: 2500,
                height: gh ,
                fill: '#ffffff', /* use transparentCorners for no fill */
                stroke: '#ffffff',
                strokeWidth: 1,
                selectable: false,
                evented: false
            });
            canvas.add(boundingbox1);
        }
        if(selectedplates == 2){
            canvas.remove(canvas.item(0));
            canvas.remove(canvas.item(1));
            canvas.remove(canvas.item(2));
            canvas.remove(canvas.item(3));
            //First Pallet   
            var boundingbox1 = new fabric.Rect({
                left: parseInt(first_pallet_left),
                top: 0,
                width: 4,
                height: gh ,
                fill: '#ffffff', /* use transparentCorners for no fill */
                stroke: '#ffffff',
                strokeWidth: 1,
                selectable: false,
                evented: false
            });
            canvas.add(boundingbox1);
            //Second Pallet   
            var boundingbox2 = new fabric.Rect({
                left: parseInt(second_pallet_total),
                top: 0,
                width: second_pallet_total + 800,
                height: gh ,
                fill: '#ffffff', /* use transparentCorners for no fill */
                stroke: '#ffffff',
                strokeWidth: 1,
                selectable: false,
                evented: false
            });
            canvas.add(boundingbox2);
        }
        if(selectedplates == 3){
            canvas.remove(canvas.item(0));
            canvas.remove(canvas.item(1));
            canvas.remove(canvas.item(2));
            canvas.remove(canvas.item(3));
            //First Pallet   
            var boundingbox1 = new fabric.Rect({
                left: parseInt(first_pallet_left),
                top: 0,
                width: 4,
                height: gh ,
                fill: '#ffffff', /* use transparentCorners for no fill */
                stroke: '#ffffff',
                strokeWidth: 1,
                selectable: false,
                evented: false
            });
            canvas.add(boundingbox1);
            //Second Pallet   
            var boundingbox2 = new fabric.Rect({
                left: parseInt(second_pallet_total),
                top: 0,
                width: 4,
                height: gh ,
                fill: '#ffffff', /* use transparentCorners for no fill */
                stroke: '#ffffff',
                strokeWidth: 1,
                selectable: false,
                evented: false
            });
            canvas.add(boundingbox2);
            //Third Pallet   
            var boundingbox3 = new fabric.Rect({
                left: parseInt(third_pallet_total),
                top: 0,
                width: parseInt(third_pallet_total + 800),
                height: gh ,
                fill: '#ffffff', /* use transparentCorners for no fill */
                stroke: '#ffffff',
                strokeWidth: 1,
                selectable: false,
                evented: false
            });
            canvas.add(boundingbox3);
        }

        p = plate;
        pdomId = p.toString();
        //jQuery("#pimg" + pdomId).css({"width": pshare[plate].toString() + "%"});
        //jQuery("#pimg" + pdomId).css({"max-width":pshare[plate].toString() + "%","object-fit":"cover","object-position": "left"});
        //console.log("pdomId.."+pdomId);
        if (pdomId == 0){
            jQuery("#pimg" + pdomId).css({"object-fit":"cover","object-position": "left"});
        }if (pdomId == 1){
            jQuery("#pimg" + pdomId).css({"object-fit":"cover","object-position": "inherit"});
        }
    }
    // ------------------------------------------------------------------------------------- define MAXIMUM snippet
    // set relative start and end according to ratio
    
    console.log("************************************ Plates:"+plateCount);
    console.log("totalWidth:"+totalPlatesWidth+" TotalHeight:"+gh);
    console.log("MotherRatio:"+motherRatio+" SelectRatio:"+selectionRatio);
    console.log("Shows:"+showWidth+"x"+showHeight);
    console.log("Rel:"+plateStartDefault);    
    // -----------------------------------------------------------------

    //Plates container width
    var plates_cont_width = parseFloat(totalPlatesWidth) / parseFloat(3); 
    //jQuery("#mainplateswrap").css({"max-width": plates_cont_width});
    var clicked_image = localStorage.getItem("choosenimg");
    console.log("clicked_image.."+clicked_image);
    if (!clicked_image){
        clicked_image = "https://dev.kumailenterprises.com/wp-content/uploads/2021/06/amusement.jpeg";
    }

    //Workign on Canvas functionality

    //36% calculation of gh height
    var calcheightpx = parseFloat(36.625) / 100 * parseFloat(gh);  
    //console.log("calpxheight.."+calcheightpx);

    canvas.setWidth(900);
    canvas.setHeight(calcheightpx);

    //console.log("Can Width.."+canvas.getWidth());
    //console.log("Can Height.."+canvas.getHeight());
    //console.log("Can Height.."+clicked_image);

    fabric.Image.fromURL(clicked_image, function(img) {
        canvas.setBackgroundImage(img, canvas.renderAll.bind(canvas),{
            id: 'canvasbg',
            originX: 'left',
            originY: 'top',
            scale: 0.4,
            width: canvas.getWidth()
        });
    });
    canvas.renderAll();

    for(plate=0; plate < plateCount; plate++) {
        p = plate;
        pdomId = p.toString();

        
        

        //Height
        //jQuery("#pimg" + pdomId).css({"Height": percalc + "px"});
        //jQuery("#pimg" + pdomId).css({"maxHeight": defaultPixelHeight[defaultType].toString() + "px"});
        //jQuery("#pimg" + pdomId).css({"maxHeight": percalc.toString() + "px"});

        //console.log("pdomId.."+pdomId);
        jQuery("#pimg" + pdomId).attr("src",bg_default_image);
        // jQuery("#pimg" + pdomId).attr("src",bg_default_image+"maker/app.php?action=getPlate&total=" +
        //     plateCount.toString() +
        //     "&no=" + plate.toString() +
        //     "&snip=" + motherSnippet.join(",") +
        //     "&pw=" + pw[p].toString() +
        //     "&platesW=" + pw.join(",") +
        //     "&h=" + gh.toString() +
        //     "&ps=" + pshare.join(",") +
        //     "&ptotal=" + totalPlatesWidth.toString() +
        //     "&default=" + defaultType +
        //     "&plateStartRel=" + plateStartDefault.join(",") +
        //     "&zoom=" + zoomMother.toString() +
        //     "&flip=" + flipMother.toString() +
        //     "&img=" + defaultImage.toString());

        // height relation = 370px = 3000mm / see default arrays!
        // //console.log("Default height: " + defaultPixelHeight[defaultType] + " - Active height: " + gh + " - Percentage: " + (parseInt(gh,10) / defaultSizesMinMax[defaultType][3] ).toString());
        //imageVerticalHeight = Math.floor(defaultPixelHeight[defaultType] * (parseInt(gh,10) / defaultSizesMinMax[defaultType][3] ));
        //if(defaultType=="kitchen") {
        //    imageVerticalHeight = Math.floor(imageVerticalHeight / plateCount);
        //}
        // //console.log("Height: " + imageVerticalHeight);
        //jQuery("#pimg" + p.toString()).css({"height": imageVerticalHeight.toString()+"px"});
        //
    }
    // jQuery.getJSON(bg_default_image+"maker/app.php?action=getImageInfo&imageId=", function(data) {
    //     defaultType = data["orientation"];
    //     matLimit = data["materialLimit"];
    //     toogleMatSelectors(matLimit);

    //     jQuery("#imageName").html(data["filename"]);
    // });
    //updatePrice();
    //setStorage();
}
// ------------------------------------------------------------------------------------------------
function cleanHeight() {
    var pdomId = "";
    var plate;

    for (plate = 0; plate < plateCount; plate++) {
        pdomId = "#pimg" + plate.toString();
        jQuery(pdomId).css({"height": "auto"});
        ////console.log("Auto: Plate " + pdomId );
    }
}
// ------------------------------------------------------------------------------------------------
function equalizeHeight() {
    var p;
    var equalHeight = 0;
    var setAtEnd = defaultPixelHeight[defaultType];
    var pdomId = "";
    var plate;

    for (plate = 0; plate < plateCount; plate++) {
        pdomId = "#pimg" + plate.toString();
        jQuery(pdomId).css({"height": "auto"});
        ////console.log("Auto: Plate " + pdomId );
    }

    if(plateCount > 1) {
        // --------------- now check
        for (plate = 0; plate < plateCount; plate++) {
            p = plate;
            pdomId = "#pimg" + p.toString();
            equalHeight = jQuery(pdomId).height();
            ////console.log("Plate " + pdomId +": " + equalHeight);
            if (equalHeight > 20) {
                if (equalHeight < setAtEnd) {
                    setAtEnd = equalHeight;
                }
            }
        }

        if(setAtEnd > 1) {
            for (plate = 0; plate < plateCount; plate++) {
                p = plate;
                pdomId = "#pimg" + p.toString();
                ////console.log("Plate " + plate + " Height:" +setAtEnd.toString())
                jQuery(pdomId).css({"height": setAtEnd.toString() + "px"});
            }
        }
    }
}
var defaultImage = 8389;
var defaultMat = 1;
var myThumbId = 0;
var myCatId = 0;
var urlRel = '/';
// Get the modal
var modal = document.getElementById("myModal");
// Get the button that opens the modal
var btn = document.getElementById("myBtn");
// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];
jQuery(document).ready(function() {
loadStorage();
jQuery( window ).resize(function() {
updateView();
});
});
function cutService() {
	modal.style.display = "inline-block";
	modal.style.position = "fixed";
	modal.style.zIndex = "99";
	jQuery(".section-content").css({"z-index":"auto"});
	initSnipper();
	return false;
}
function setService(myVal) {
	jQuery("input[name='snippetService']:checked").val(myVal);
	if(myVal=="ProfiSnippet") {
	jQuery("#ProfiSnippet").addClass("matactive");
	jQuery("#SelfSnippet").removeClass("matactive");
	} else {
	jQuery("#SelfSnippet").addClass("matactive");
	jQuery("#ProfiSnippet").removeClass("matactive");
	}
}
function scrollCatRight() {
	//jQuery("#matLeftArrow").show();
	var x = jQuery("#catHeader").scrollLeft();
	var newX = x + 100;
	jQuery("#catHeader").scrollLeft(newX);
	if(x == jQuery("#catHeader").scrollLeft()) {
	//jQuery("#matRightArrow").hide();
	}
}
function scrollCatLeft() {
	// jQuery("#matRightArrow").show();
	var x = jQuery("#catHeader").scrollLeft();
	var newX = x - 100;
	if(newX < 0) {
	newX = 0;
	}
	jQuery("#catHeader").scrollLeft(newX);
	if(newX == 0) {
	// jQuery("#matLeftArrow").hide();
	}
}
function scrollImgRight() {
	//jQuery("#matLeftArrow").show();
	var x = jQuery("#imagesHeader").scrollLeft();
	var newX = x + 120;
	jQuery("#imagesHeader").scrollLeft(newX);
	if(x == jQuery("#imagesHeader").scrollLeft()) {
	//jQuery("#matRightArrow").hide();
	}
}
function scrollImgLeft() {
	// jQuery("#matRightArrow").show();
	var x = jQuery("#imagesHeader").scrollLeft();
	var newX = x - 120;
	if(newX < 0) {
	newX = 0;
	}
	jQuery("#imagesHeader").scrollLeft(newX);
	if(newX == 0) {
	// jQuery("#matLeftArrow").hide();
	}
}
//
function setStorage() {
	if (typeof (Storage) !== "undefined") {
	localStorage.setItem("lastCatId", myCatId);
	localStorage.setItem("lastImageId", defaultImage);
	localStorage.setItem("lastMatId", defaultMat);
	localStorage.setItem("lastHeight", totalPlatesHeight);
	localStorage.setItem("lastWidth", totalPlatesWidth);
	localStorage.setItem("lastW0", parseInt(jQuery("#width0").val(),10));
	localStorage.setItem("lastW1", parseInt(jQuery("#width1").val(),10));
	localStorage.setItem("lastW2", parseInt(jQuery("#width2").val(),10));
	localStorage.setItem("lastPlates", plateCount);
	}
}
function loadStorage() {
	if (typeof (Storage) !== "undefined") {
	if( 0 + parseInt(localStorage.getItem("lastCatId"),10) > 0 ) {
	// set last values
	jQuery("#width0").val(parseInt(localStorage.getItem("lastW0"),10));
	jQuery("#width1").val(parseInt(localStorage.getItem("lastW1"),10));
	jQuery("#width2").val(parseInt(localStorage.getItem("lastW2"),10));
	jQuery("#pHeight").val(parseInt(localStorage.getItem("lastHeight"),10));
	plateCount = parseInt(localStorage.getItem("lastPlates"),10)
	myThumbId = 0 + parseInt(localStorage.getItem("lastImageId"),10);
	//jQuery.getJSON(urlRel+"maker/app.php?action=getImageInfo&imageId=" + myThumbId.toString(), function(data) {
	// defaultType = data["orientation"];
	// jQuery("#imageName").html(data["filename"]);
	// calcStartValues();
	// catSelect(parseInt(localStorage.getItem("lastCatId"),10));
	// //console.log("Storage values found");
	// });
	} else {
	myCatId = 41;
	//console.log("Cat ID found");
	calcStartValues();
	changePlates(2);
	}
	//jQuery.getJSON(urlRel+"maker/app.php?action=getImageInfo&imageId=" + defaultImage.toString(), function(data) {
	// jQuery("#imageName").html(data["filename"]);
	// });
	} else {
	myCatId = 41;
	calcStartValues();
	changePlates(2);
	}
}
function clearSettings() {
	if (typeof (Storage) !== "undefined") {
	localStorage.clear();
	location.reload();
	}
}

});